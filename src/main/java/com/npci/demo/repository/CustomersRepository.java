package com.npci.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.npci.demo.entity.Customers;

public interface CustomersRepository extends JpaRepository<Customers, Integer> {

}
