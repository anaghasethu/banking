package com.npci.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.npci.demo.dao.BankingDao;
import com.npci.demo.entity.Customers;
import com.npci.demo.repository.CustomersRepository;
import com.npci.demo.service.BankingService;

@Service
public class BankingServiceImpl implements BankingService {

//	@Autowired BankingDao bankingDao;
	@Autowired CustomersRepository customersRepository;
	@Override
	public Customers addCustomer(Customers customers) {
		// TODO Auto-generated method stub
		return customersRepository.save(customers);
	}
	
}
