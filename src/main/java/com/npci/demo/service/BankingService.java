package com.npci.demo.service;

import org.springframework.stereotype.Service;

import com.npci.demo.entity.Customers;

@Service
public interface BankingService {

	public Customers addCustomer(Customers customers);

}
